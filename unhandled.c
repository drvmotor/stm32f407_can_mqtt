/* This is a source file unhandled.c
 TITLE:
 LICENSE:

 * Copyright 2016-2022 drvmotor <100kwmore@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 Author: (c)drvmotor
 Date:09/01/22
*/

/*
 * If a serious error occurs, one of the fault
 * exception vectors in this file will be called.
 *
 * This file attempts to aid the unfortunate debugger
 * to blame someone for the crashing code
 *
 *  Created on: 12.06.2013
 *      Author: uli
 *
 * Released under the CC0 1.0 Universal (public domain)
 */
#include <stdint.h>
#include <ch.h>
#include <string.h>

#define STM32F407xx 1
#include "etc/nosys/stm32f4xx.h"
/**
 * Executes the BKPT instruction that causes the debugger to stop.
 * If no debugger is attached, this will be ignored
 */
#define bkpt() __asm volatile("BKPT #0\n")

static char *post = (void *)SRAM1_BASE;
static inline __attribute__((always_inline)) void post_diag(const char *txt)
{
	char *p = post;
	while (*txt) *p++ = *txt++;
    bkpt();
    NVIC_SystemReset();
}

void NMIVector(void) 			{ post_diag("NMI"); }
void HardFaultVector(void)	{ post_diag("HARD"); }
void BusFaultVector(void)		{ post_diag("BUS"); }
void UsageFaultVector(void)	{ post_diag("USAGE"); }
void MemManageVector(void) 	{ post_diag("MEMMG"); }

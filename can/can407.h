/* This is a header file can407.h
 TITLE: This is a part of mqtt_can project
 LICENSE:

 * Copyright 2016-2022 drvmotor <100kwmore@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 Author: (c)drvmotor
 Date:08/29/22
*/
#ifndef _CAN407_H_
#define _CAN407_H_ 1

#ifndef CAN1_ENABLED
#define CAN1_ENABLED 1
#endif

#define CAN2_ENABLED 1
extern unsigned int pack_sent, pack_recv, err_mqttsub;

void nosys_can_init(int num);
void can_serve(void);

#ifdef CAN2_ENABLED
void can2test(void);
#endif

#endif


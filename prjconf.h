/* This is a header file prjconf.h
 TITLE: This is a part of mqtt project
 LICENSE:

 * Copyright 2016-2022 drvmotor <100kwmore@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 Author: (c)drvmotor
 Date:08/31/22
*/
#ifndef _PRJCONF_H_
#define _PRJCONF_H_

#define BLINKER_PORT GPIOE
#define BLINKER_PIN  (15)
#define MQTT_ALARM_PIN (14)

/*server data */
#define MQTT_SERVER_ADDR "192.168.1.161"
#define MQTT_SERVER_PORT (1883)
#define MQTT_SERVER_LOGIN "mqtt"
#define MQTT_SERVER_PASSWORD "mqtt"
#define MQTT_PUB_TOPIC "myhome/stm32/CAN1"
#define MQTT_SUB_TOPIC "myhome/stm32/CAN1/set"
#define MQTT_TRYCONNECT_TIME (5)

#define CAN_BAUD (125000)

#if 0
/*prev version*/
#define MQTT_SERVER_ADDR "192.168.1.2"
#define MQTT_SERVER_PORT (1883)
#define MQTT_SERVER_LOGIN "home"
#define MQTT_SERVER_PASSWORD "123qwe"
#define MQTT_PUB_TOPIC "myhome/CAN1"
#define MQTT_SUB_TOPIC "myhome/CAN1/#"
#define MQTT_TRYCONNECT_TIME (5)

#define CAN_BAUD (125000)
#endif

/*utils*/
extern void 	mqtt2can(int len, char *data, int topiclen, char *topic);
extern void 	can2mqtt(void *data, int dlc, unsigned int ID, char isExID);

extern int  	mqtt_publisher(int len, char *data, char *topic);
extern void  	myprintf(const char *fmt, ... );
extern int 		db_exist(void);
extern char* db_topic_stem(void);
#endif


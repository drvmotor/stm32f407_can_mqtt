#include "ch.h"
#include "hal.h"

#include "chprintf.h"
#include "MQTTClient.h"
#include "mqtt.h"
#include <string.h>
#include "../prjconf.h"
#if LWIP_NETCONN


void messageArrived(MessageData* md)
{
	/*get topic name and call the handler upper layer*/
  MQTTMessage* message = md->message;
  int tlen = 0;
  char *tptr=NULL;
  if (md->topicName->cstring != NULL)
	{
		tptr = md->topicName->cstring;
		tlen = strlen(tptr);
	}
	else
	{
		tptr = md->topicName->lenstring.data;
		tlen = md->topicName->lenstring.len;
	}
	mqtt2can( (int) message->payloadlen, (char *) message->payload, tlen, tptr);
}

WORKING_AREA(wa_mqtt_client, MQTT_THREAD_STACK_SIZE);

static Client *cl_mirror;

msg_t mqtt_client(void *p)
{
  int rc = 0;
  Network n;
  Client c;
  unsigned char buf[100];
  unsigned char readbuf[100];

  (void)p;

	for(;;)
	{
	  /* Create a new TCP connection handle */
		NewNetwork (&n);
		chprintf((void *) &SD6, "Connecting to %s\r\n", MQTT_SERVER_ADDR);
	    chThdSleepMilliseconds(50);
		rc = ConnectNetwork (&n, MQTT_SERVER_ADDR, MQTT_SERVER_PORT);

		if (rc != 0)
		{
			chprintf((void *) &SD6, "Failed to connect to server, code %d\r\n", rc);
			n.disconnect(&n);
			goto blink_to_death;
		}

	    chThdSleepMilliseconds(50);
		chprintf((void *) &SD6, "Initializing MQTT service\r\n");
		MQTTClient (&c, &n, 1000, buf, 100, readbuf, 100);
		MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
		data.willFlag = 0;
		data.MQTTVersion = 3;

	  data.clientID.cstring = MQTT_SERVER_LOGIN;
	  data.username.cstring = MQTT_SERVER_LOGIN;
	  data.password.cstring = MQTT_SERVER_PASSWORD;

	  data.keepAliveInterval = 5;
	  data.cleansession = 1;

	  rc = MQTTConnect (&c, &data);

	  if (rc == MQTT_FAILURE)
	  {
	    chprintf((void *) &SD6, "Failed to send connect request\r\n");
	    goto blink_to_death;
	  }

	  chprintf((void *) &SD6, "Subscribing to its topic\r\n");

	if(db_exist())
	{
	  rc = MQTTSubscribe(&c, db_topic_stem(), QOS0, messageArrived);
	}
	else
	{
	  rc = MQTTSubscribe(&c, MQTT_SUB_TOPIC, QOS0, messageArrived);
	}

	  if (rc == MQTT_FAILURE)
	  {
	    chprintf((void *) &SD6, "Failed to subscribe to its topic\r\n");
	    MQTTDisconnect(&c);
	    goto blink_to_death;
	  }

	  chprintf((void *) &SD6, "Connected to %s\r\n", MQTT_SERVER_ADDR);

	  cl_mirror = &c;

	  while(1)
	  {
	    MQTTYield(&c, 1000);
	  }


	blink_to_death:
		
	  for(int j=0; j < 10*MQTT_TRYCONNECT_TIME; ++j)
	  {
	    palClearPad(BLINKER_PORT, MQTT_ALARM_PIN);
	    chThdSleepMilliseconds(50);
	    palSetPad(BLINKER_PORT, MQTT_ALARM_PIN);
	    chThdSleepMilliseconds(50);
	  }
		palSetPad(BLINKER_PORT, MQTT_ALARM_PIN);
	}

	return RDY_OK;
}

int mqtt_publisher(int len, char *data, char *topic)
{
	MQTTMessage msg;
	if(cl_mirror == NULL) return 0;
	msg.qos = QOS0;
	msg.payloadlen = (size_t) len;
	msg.payload = data;
	msg.retained = msg.dup = 0;
	return !MQTTPublish(cl_mirror, topic, &msg);
}

#endif /* LWIP_NETCONN */

/** @} */

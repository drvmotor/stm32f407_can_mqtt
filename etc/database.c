/* This is a source file database.c
 TITLE: This is a part of mqtt2 project
 LICENSE:

 * Copyright 2016-2023 drvmotor <drvmotor@i.ua>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 Author: (c)drvmotor
 Date:04/11/23
*/
#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <ctype.h>

#include "database.h"
#include "db_phy.h"


static const char *crlf="\r\n";
static char strbuf[512];
static size_t seeking;


enum db_requests db_request(char *buf, size_t *pos)
{
	enum db_requests rrr = rq_none;

	if(!strncmp(buf, "$$$GET", 6))
	{
		rrr = rq_get;
		*pos = 6;
	}
	else if(!strncmp(buf, "$$$PUT", 6))
	{
		*pos = 6;
		rrr= rq_put;
	}
	else if(!strncmp(buf, "$$$FIN", 6))
	{
		*pos = 6;
		rrr= rq_fin;
	}
	return rrr;
}


static const char *nodata="NONE\r\n";
static const char *written_data="written\r\n";

static void fwd_parse(struct dbRecord *item);

char *db_get_item(int item)//return asciiz string
{
	if(item < 0) return nodata;
	struct dbRecord *ptr = db_item(item);
	if(ptr == NULL) return nodata;
	/*make csv*/
	if(!item)
	{
		//strcpy(strbuf, ptr->text); // accelerate stem
		snprintf(strbuf, sizeof(strbuf)-1, "%s\r\n", ptr->text);
	}
	else
	{
		fwd_parse(ptr);
	}

	return strbuf;
}

static void back_parse(struct dbRecord *item, char *s);
static int isStem(struct dbRecord *item);

char *db_put_item(char *buf)// return *data to reply, input item # and asciiz string
{
	struct dbRecord newrec;
	int stem = 0;
	/*crlf hiding*/
	while(*buf && isspace(*buf)) buf++;
	char *p = buf;
	while(*p) 
	{
		if((*p == '\r')||(*p == '\n')) 
		{
			*p = 0;
			break;
		}
		p++;
	}
	
	/*check if is stem*/
	if((buf[0] == 'S')
		&& (buf[1] == 'T')
		&& (buf[2] == 'E')
		&& (buf[3] == 'M')
		&& (buf[4] == '='))
	{//is a stem
		memset(&newrec, 0, sizeof(newrec));
		strcpy(newrec.text, &buf[5]);
		stem = 1;
	}
	else
	{
		back_parse(&newrec, buf);
	}

	db_write_item(&newrec, sizeof(newrec), stem);
	return written_data;
}


void fwd_parse(struct dbRecord *item)
{
	char abbrev[64], topic[64], comment[64];

	memcpy(abbrev, item->text, item->abbrev_len);
	abbrev[item->abbrev_len] = 0;
	memcpy(topic, &item->text[item->abbrev_len], item->topic_len);
	topic[item->topic_len]=0;
	memcpy(comment, &item->text[item->abbrev_len+item->topic_len], item->comment_len);
	comment[item->comment_len] = 0;
/*
	create portion of 512 bytes data for tcp
	stid mask format min max abbrev topic comment
	csv format
*/
    snprintf(strbuf, sizeof(strbuf)-1,
		"0x%04X;0x%04X;%c;0x%04lX;0x%04lX;%s;%s;%s\r\n",
		item->stID, item->mask, item->format, item->min, item->max,
		abbrev, topic, comment);
}


static char *get_cell(char *dst, char *src, char delim)
{
	char *rrr = NULL;
	*dst = 0;
	if(src == NULL) return NULL;
	for(;;)
	{
		*dst = 0;
		if(*src == 0) break;
		if(*src == delim)
		{ 
			rrr = &src[1];
			break;
		}
		
		if(*src != ' ')
		{
			*dst++ = *src;
		}
			
		src++; 
	}
	return rrr;
}


void back_parse(struct dbRecord *item, char *s)
{
	char *cell = item->text;
	memset(item, 0, sizeof(struct dbRecord));
	s = get_cell(cell, s, ';'); 
	item->stID = strtoul(cell,  NULL, 0);
	s = get_cell(cell, s, ';'); 
	item->mask = strtoul(cell, NULL, 0);
	s = get_cell(cell, s, ';');
	item->format = cell[0];

	/*min max*/
	switch(item->format)
	{
		case 'b':
		case 'd':
		case 'L':
		case 'F':
		case 'f':
			s = get_cell(cell, s, ';');
			item->min = strtol(cell, NULL, 0);
			s = get_cell(cell, s, ';');
			item->max = strtol(cell, NULL, 0);
		break;
		
		default:
		
			s = get_cell(cell, s, ';');
			item->min = strtoul(cell, NULL, 0);
			s = get_cell(cell, s, ';');
			item->max = strtoul(cell, NULL, 0);
		break;
	}
	
	s = get_cell(cell, s, ';'); //abbr
	item->abbrev_len = strlen(cell);

	cell = &item->text[item->abbrev_len];
	s = get_cell(cell, s, ';'); //topic

	item->topic_len = strlen(cell);
	/*last cell*/
	item->comment_len = strlen(s);
	strcpy(&item->text[item->abbrev_len + item->topic_len], s);
}


struct dbRecord *db_item(int item)
{
	if(item < 0) return NULL;
	if(item == 0) db_open_read(); // refresh db
	return db_read_item(sizeof(struct dbRecord) * item);
}

static void db_compacting(void);
void db_finalize(void)
{
	db_open_read();
	db_compacting();
	db_write_close();
}

int 		db_exist(void)
{
	return db_read_item(0) != NULL;
}

char* db_topic_stem(void)
{
	struct dbRecord *cell = db_item(0);
	return (cell == NULL)? NULL : cell->text;
}

void db_compacting(void)
{
	struct dbRecord *img = db_get_image();
	if(img == NULL) return;
	size_t nelem = db_get_items_to_wr(sizeof(struct dbRecord));
	if(nelem == 0) return;
	
	// reserved 
}

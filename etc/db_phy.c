/* This is a source file db_phy.c
 TITLE: This is a part of mqtt2 project
 LICENSE:

 * Copyright 2016-2023 drvmotor <drvmotor@i.ua>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 Author: (c)drvmotor
 Date:04/11/23
*/
#include <string.h>
#include "nosys/stm32f4xx.h"
#include "db_phy.h"

/*find the first record*/
static const uint32_t __attribute__((aligned(4))) signature = 0xDEADBEEF;
static const uint32_t arch_pagesize = 128*1024;
/*db record infos*/
static struct db_info_s
{
	uint8_t *valid;
	size_t  sz_used, sz_free;

	uint8_t *img;
	size_t  wr_pos;

	uint32_t	base;
	uint32_t	last;
	uint32_t	unused;
} db_info;




size_t 		db_get_items_to_wr(size_t el_size)
{
	return db_info.wr_pos/el_size;
}

void		*db_get_image(void)
{
	return db_info.img;
}





static void *sgn_find(uint32_t from)
{
	uint32_t *p = (uint32_t *) from;
	while(*(--p) != signature)
		if((uint32_t) p <= db_info.base)
			return NULL;
	return p;
}

int 		db_open_read(void)
{
	uint32_t base = FLASH_BASE;
	uint32_t romsize = *((uint16_t *) FLASHSIZE_BASE) * 1024;
	db_info.last = base +  romsize;
	db_info.base = db_info.last - arch_pagesize;

	/*find signature*/
	uint32_t *p = sgn_find(db_info.last);
	if(p == NULL)
	{
		db_info.unused = db_info.base;
		db_info.sz_free = arch_pagesize;
		db_info.sz_used = 0;
		return 0;
	}
	else
	{
		//found 1st signature
		db_info.unused = (uint32_t) &p[1];
		uint32_t *q = sgn_find((uint32_t)p);
		// if next sgn == 0 then base is valid
		if(q == NULL)
			db_info.valid = (uint8_t *) db_info.base;
		else
			db_info.valid = (uint8_t *) (&q[1]);
		uint32_t bbb = (uint32_t) db_info.valid;
		//db_info.sz_used = arch_pagesize - (bbb - db_info.base);
		db_info.sz_used =  db_info.unused - (uint32_t) db_info.valid;
		db_info.sz_free = arch_pagesize - (db_info.unused - db_info.base);
	}
	return 1;
}


void		*db_read_item(size_t item) // if no data then null
{
	//checking no data condition
	if((item >= (db_info.sz_used - sizeof(signature))) || (db_info.valid == NULL)) return NULL;
	return &db_info.valid[item];
}

static void  db_open_write(void)
{
	db_info.img = (uint8_t *) CCMDATARAM_BASE;
	db_info.wr_pos = 0;
}

void db_write_item(void *data, size_t sz, int seek_zero)
{
	if((db_info.img == NULL) || seek_zero) db_open_write();
	memcpy(&db_info.img[db_info.wr_pos], data, sz);
	db_info.wr_pos += sz;
}


static void flash_wait_for_last_operation(void)
{
	while (FLASH->SR & FLASH_SR_BSY);
}


#define Pcast(d,s,ty) *(ty *) d = *(ty *) s;
static void flash_program_data(void *dest, void *src, size_t sz)
{
	flash_wait_for_last_operation();
	FLASH->CR &= ~FLASH_CR_PSIZE;
	switch(sz)
	{
		case 1:
			FLASH->CR |= FLASH_CR_PG;
			Pcast(dest, src, uint8_t);
		break;
		case 2:
			FLASH->CR |= FLASH_CR_PSIZE_0;
			FLASH->CR |= FLASH_CR_PG;
			Pcast(dest, src, uint16_t);
		break;
		case 4:
			FLASH->CR |= FLASH_CR_PSIZE_1;
			FLASH->CR |= FLASH_CR_PG;
			Pcast(dest, src, uint32_t);
		break;
		case 8:
			FLASH->CR |= FLASH_CR_PSIZE;
			FLASH->CR |= FLASH_CR_PG;
			Pcast(dest, src, uint64_t);
		break;
		default: return;
	}

	db_info.sz_used += sz;
	db_info.sz_free -= sz;
	db_info.unused +=  sz; // point to unused area
	flash_wait_for_last_operation();
	FLASH->CR &= ~FLASH_CR_PG;
}

int 	 	db_write_close(void)
{
	uint8_t *destPTR = NULL;
	/*unlock flash*/
	FLASH->KEYR = 0x45670123; //key 1
	FLASH->KEYR = 0xcdef89ab; //key 2

	if(db_info.sz_free < db_info.wr_pos)
	{//img not fits to sector& perform erase
		flash_wait_for_last_operation();
		FLASH->CR |= FLASH_CR_PSIZE;
		FLASH->CR &= ~FLASH_CR_SNB;
		FLASH->CR |= FLASH_CR_SNB_0 * 11;//last sector
		FLASH->CR |= FLASH_CR_SER;
		FLASH->CR |= FLASH_CR_STRT;
		flash_wait_for_last_operation();
        FLASH->CR &= ~FLASH_CR_SER;
        db_info.unused = db_info.base;
        db_info.sz_used = 0;
        db_info.sz_free = arch_pagesize;
	}
	else
	{
		destPTR = (uint8_t *) db_info.unused;
		if(destPTR == NULL) destPTR = db_info.base;
		
	}
    
    
	/*write image*/
	flash_wait_for_last_operation();

	for(size_t idx = 0; idx < db_info.wr_pos; ++idx)
	{
		flash_program_data(destPTR++, &db_info.img[idx], 1);
	}

	/*place signature*/
	if((uint32_t) destPTR & 3)
	{	/*check alignment*/
		uint32_t d = (uint32_t) destPTR | 3;;
		destPTR = (uint8_t *) d + 1;
	}

	flash_program_data(destPTR, &signature, sizeof(signature));
	/*lock flsh back*/
	FLASH->CR |= FLASH_CR_LOCK;
	return 1;
}




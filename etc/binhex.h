/* This is a header file binhex.h
 TITLE: This is a part of mqtt2 project
 LICENSE:

 * Copyright 2016-2023 drvmotor <drvmotor@i.ua>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 Author: (c)drvmotor
 Date:04/12/23
*/
#ifndef _BINHEX_H_
#define _BINHEX_H_ 1
#include <stdint.h>
char fast_binhex4(uint8_t b);
char *fast_binhex8(char *txt, uint8_t byte);
char *fast_binhex16(char *txt, uint16_t w);
char *fast_binhex32(char *txt, uint32_t w);

uint8_t fast_hexbin4(char c);
char *fast_hexbin8(char *txt, uint8_t *val);
char *fast_hexbin16(char *txt, uint16_t *val);
char *fast_hexbin32(char *txt, uint32_t *val);

#endif


/* This is a source file handlers.c
 TITLE: This is a part of mqtt2 project
 LICENSE:

 * Copyright 2016-2023 drvmotor <drvmotor@i.ua>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 Author: (c)drvmotor
 Date:04/13/23
*/

/*translate mqtt packet to can */
#include <ctype.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "binhex.h"
#include "can407.h"
#include "database.h"


static int mmin(int a, int b)
{
	return (a < b)? a:b;
}


static int mmax(int a, int b)
{
	return (a > b)? a:b;
}

static void printfloats(char *dest, void *data, int bytes, int decimals)
{
	int32_t v1=0; memcpy(&v1, data, mmax(bytes, sizeof(v1)));
	float denom = (float) v1;
	char *fmtspec="%.3f";
	fmtspec[2]= fast_binhex4(decimals);
	for(int j=0; j < decimals;++j)
		denom /= 10.0;
	sprintf(dest, fmtspec, denom);
}

void 	can2mqtt(void *data, int dlc, unsigned int ID, char isExID)
{
	char topic[128];
	char content[128];
	int itemN = 0;
	/*get stem*/
	struct dbRecord *db = db_item(itemN++);
	if(db == NULL) return; //db not exist
	topic[0]=content[0] = 0;
	strncat(topic, db->text, db->topic_len);
	/*find by id and mask*/
	for(;;)
	{
		db = db_item(itemN++);
		if(db == NULL) return; // record not found
		/*test for record*/
		if((ID & db->mask) == db->stID)
		{
			if(db->min == db->max)
			{
				uint8_t *element = data;
				if(*element != db->min) continue;
			}
			/*otherwise item found*/
			break;
		}
	}
	/*record found*/
	strncat(topic,&db->text[db->abbrev_len], db->topic_len);
	if(db->min == db->max)
	{
		strncpy(content, db->text, db->abbrev_len);
	}
	else
	{ // get format
		char *fmt = "%lu";
		switch(db->format)
		{
			case 'n': //bit value on off
				if(*(char*)data) strcat(content,"on");
				else strcat(content,"off");
				break;

			case 'c': //u8
				strncat(content, data, dlc);
				break;
			case 'b': //i8
				sprintf(content, "%d", (int) (*(char*)data));
				break;
			case 'd': //i16
				{
					int16_t d;
					memcpy(&d, data, mmax(dlc, sizeof(int16_t)));
					sprintf(content, "%d", d);
				}
				break;
			case 'u': //u16
				{
					uint16_t u;
					memcpy(&u, data, mmax(dlc, sizeof(uint16_t)));
					sprintf(content, "%u", u);
				}
				break;
			case 'f': //fp32 2 digits after point
				printfloats(content, data, dlc, 2);
				break;
			case 'F': //fp32 3 digits after point
				printfloats(content, data, dlc, 3);
				break;
			case 'L': //i32
				fmt[2] = 'd';
			case 'U': //u32
			default:
			{
				uint32_t value= 0;
				memcpy(&value, data, mmax(dlc, sizeof(value)));
				sprintf(content, fmt, value);
			}
				break;
		}
	}
	mqtt_publisher(strlen(content), content, topic);
}

static void pasteN(uint8_t *buf, uint32_t var, size_t sz)
{
	if(sz >= 1) *buf++ = (uint8_t) var;
	if(sz >= 2) *buf++ = (uint8_t) (var >> 8);
	if(sz >= 3) *buf++ = (uint8_t) (var >> 16);
	if(sz == 4) *buf++ = (uint8_t) (var >> 24);
}
/*reads topic data and transforms it to can packet, then sends*/
void mqtt2can(int len, char *data, int topiclen, char *topic)
{
	uint8_t buf[8];
	int dlc;
	int itemN = 0;
	struct dbRecord *db = db_item(itemN++);

	if(db == NULL) return; //db not exist

	char *stem = db->text;
	size_t stem_len = db->topic_len;

	while(isspace(*data) && len)
	{ //left spaces trim
		data++; len--;
	}

	for(;;)
	{
		db = db_item(itemN++);
		if(db == NULL) return; // no send
		if(strncmp(topic, stem, stem_len)) continue;
		if(strncmp(&topic[stem_len], &db->text[db->abbrev_len], db->topic_len)) continue;
		/*found corresponding item*/
		if(!len)
		{
			buf[0]=0; dlc = 0;
			break;
		}

		if(db->min == db->max)
		{ // enumeration
			if(!strncmp(&db->text, data, mmin(len, db->abbrev_len)))
			{//extract enum data
				buf[0]=db->min;
				dlc = 1;
				break;
			}
			else continue;
		}

		// another values
		dlc = 4;
		switch(db->format)
		{
			case 'n': //bit value on off
			if(len >= 2)
			{
				int dub = -1;
				if(!strncasecmp(data, "ON", 2)) dub = 1;
				else if(!strncasecmp(data,"OFF",3)) dub = 0;
				if(dub >= 0)
				{
					buf[0]=dub;
					dlc = 1;
				}
			}
			break;
			case 'c': //u8
			case 'b': //i8
				dlc = 1;
				goto cmnn;
			case 'd': //i16
			case 'u': //u16
				dlc = 2;
			cmnn:
			{
				int ddd = atoi(data);
				if(
					((db->format == 'd')||(db->format == 'b'))
					&& (ddd < 0)
				) ddd &= 0xFF;

				pasteN(buf, ddd, 2);
			}
			break;
			case 'f': //fp32 2 digits after point
				pasteN(buf, (int)(0.5 + atof(data) * 100), dlc);
			break;
			case 'F': //fp32 3 digits after point
				pasteN(buf, (int)(0.5 + atof(data) * 1000), dlc);
			break;
			case 'L': //i32
				pasteN(buf, strtol(data,NULL,10), dlc);
			break;
			case 'U': //u32
			default:
				pasteN(buf, strtoul(data,NULL,10), dlc);
			break;
		}
		break;
	}
	/*incoming data is transformed to buf and dlc*/
	char extid = 0;
	for(uint32_t iden = 0; iden <= 0xFFFF; ++iden)
	{
		if((iden & db->mask) != db->stID) continue;
		if(can_send(1, buf, dlc, iden, extid))
			pack_sent += 1;
	}
}

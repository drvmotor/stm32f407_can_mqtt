/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/**
 * @file web.c
 * @brief TCP server wrapper thread code.
 * @addtogroup WEB_THREAD
 * @{
 */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "ch.h"
#include "hal.h"

#include "chprintf.h"
#include "lwip/opt.h"
#include "lwip/arch.h"
#include "lwip/api.h"

#include "web.h"
#include "database.h"

#if LWIP_NETCONN

static const char http_html_hdr[] = "HTTP/1.1 200 OK\r\nContent-type: text/html\r\n\r\n";
static const char http_index_html[] = "<html><head><title>Congrats!</title></head><body><h1>Welcome to our lwIP HTTP server!</h1><p>This is a small test page.</body></html>";


static void http_server_serve(struct netconn *conn, char *buf, u16_t buflen)
{// old server by default     
	/* Is this an HTTP GET command? (only check the first 5 chars, since there are other formats for GET, and we're keeping it very simple )*/
    if (buflen>=5 &&
        buf[0]=='G' &&
        buf[1]=='E' &&
        buf[2]=='T' &&
        buf[3]==' ' &&
        buf[4]=='/' ) 
	{
      /* Send the HTML header
             * subtract 1 from the size, since we dont send the \0 in the string
             * NETCONN_NOCOPY: our data is const static, so no need to copy it
       */

      netconn_write(conn, http_html_hdr, sizeof(http_html_hdr)-1, NETCONN_NOCOPY);
      /* Send our HTML page */
      netconn_write(conn, http_index_html, sizeof(http_index_html)-1, NETCONN_NOCOPY);
    }
}





static void tcp_server_serve(struct netconn *conn)
{
	struct netbuf *inbuf;
	char *buf;
	u16_t buflen;
	err_t err;
	/* Read the data from the port, blocking if nothing yet there.
	We assume the request (the part we care about) is in one netbuf */
	err = netconn_recv(conn, &inbuf);

	if (err == ERR_OK)
	{
		size_t index = 0;
		netbuf_data(inbuf, (void **)&buf, &buflen);
		switch(db_request(buf, &index))
		{
			case rq_get:
			{
				char *s = db_get_item(atoi(&buf[index]));
				netconn_write(conn, s, strlen(s), NETCONN_NOCOPY);
			}
				break;
			case rq_put:
			{
				char *s = db_put_item(&buf[index]);
				netconn_write(conn, s, strlen(s), NETCONN_NOCOPY);
			}
				break;
			case rq_fin:
				db_finalize();
				break;
			case rq_none:
			default:
				http_server_serve(conn, buf, buflen);
				break;
		}
	}
  /* Close the connection (server closes in HTTP) */
  netconn_close(conn);
  /* Delete the buffer (netconn_recv gives us ownership,
   so we have to make sure to deallocate the buffer) */
  netbuf_delete(inbuf);
}

/**
 * Stack area for the http thread.
 */
WORKING_AREA(wa_http_server, WEB_THREAD_STACK_SIZE);

/**
 * HTTP server thread.
 */
	msg_t http_server(void *p) {
	struct netconn *conn, *newconn;
	err_t err;
	(void)p;

	/* Create a new TCP connection handle */
	conn = netconn_new(NETCONN_TCP);
	LWIP_ERROR("http_server: invalid conn", (conn != NULL), return RDY_RESET;);
	chprintf((void *) &SD6, "Running http server\r\n");
	/* Bind to port 80 (HTTP) with default IP address */
	netconn_bind(conn, NULL, WEB_THREAD_PORT);
	/* Put the connection into LISTEN state */
	netconn_listen(conn);
	/* Goes to the final priority after initialization.*/
	chThdSetPriority(WEB_THREAD_PRIORITY);

	while(1)
	{
		chprintf((void *) &SD6, "Waiting for connection\r\n");
		err = netconn_accept(conn, &newconn);
		if (err != ERR_OK)
			continue;

		tcp_server_serve(newconn);
		netconn_delete(newconn);
	}
	return RDY_OK;
}

#endif /* LWIP_NETCONN */

/** @} */

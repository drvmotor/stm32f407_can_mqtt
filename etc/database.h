/* This is a header file database.h
 TITLE: This is a part of mqtt2 project
 LICENSE:

 * Copyright 2016-2023 drvmotor <drvmotor@i.ua>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 Author: (c)drvmotor
 Date:04/11/23
*/
#ifndef _DATABASE_H_
#define _DATABASE_H_ 1
#include <stddef.h>
#include <stdint.h>
/*
	record 0 contains topic stem with actual length in topic_len
*/
struct dbRecord
{
	uint16_t stID;
	uint16_t mask;
	uint32_t min;
	uint32_t max;
	char	 format;
	uint8_t  abbrev_len;
	uint8_t  topic_len;
	uint8_t  comment_len;
	char	 text[128];
};


enum db_requests
{
	rq_none, rq_get, rq_put, rq_fin
};

enum db_requests db_request(char *buf, size_t *pos);
char *db_get_item(int item);//return NULL if no data otherwise asciiz string
char *db_put_item(char *buf);// return *data to reply, input item # and asciiz string
void db_finalize(void);
struct dbRecord *db_item(int item);
int 		db_exist(void);
#endif


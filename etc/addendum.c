/* This is a source file addendum.c
 TITLE: This is a part of mqtt2 project
 LICENSE:

 * Copyright 2016-2023 drvmotor <100kwmore@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 Author: (c)drvmotor
 Date:03/29/23
*/
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "addendum.h"

#include "serial.h"
#include "nosys/stm32f4xx.h"


struct dbRecord
{
	uint16_t stID;	
	uint16_t mask;
	char	 format;
	uint32_t min;
	uint32_t max;
	uint8_t  abbrev_len;
	uint8_t  topic_len;
	uint8_t  comment_len;
	char	 text[128];
};


static char strbuf[512];
static size_t seeking;

static int  read_database(struct dbRecord *rec, size_t index);
static int  write_database(struct dbRecord *rec, size_t index);
static void close_database(size_t index);




/*find the first record*/
static const uint32_t __attribute__((aligned(4))) signature = 0xDEADBEEF;
static const uint32_t arch_pagesize = 128*1024;
static uint32_t db_base(void)
{
	uint32_t romsize = *((uint16_t *) FLASHSIZE_BASE) * 1024;
	return FLASH_BASE + romsize - arch_pagesize; // last pagesize
}

static uint32_t db_limit(void)
{
	return db_base() + arch_pagesize;
}


static void *db_head(void)
{
	uint32_t *last = (uint32_t *)db_base();
	for(uint32_t j=0; j < (arch_pagesize/sizeof(uint32_t)) - 1; ++j)
	{
		if(last[j] == signature)
		{
			return &last[j+1];
		}
	}
	return last;
}

static int sector_blank_check(void)
{
	uint32_t ddd = 0xFFFFFFFF;
	uint32_t *ptr = (uint32_t *) db_base();
	for(size_t j=0; j<(arch_pagesize/sizeof(uint32_t)); ++j)
	{
		ddd &= ptr[j];
	}
	return (ddd == 0xFFFFFFFF)? 1:0;
}



static int  read_database(struct dbRecord *rec, size_t index)
{
	struct dbRecord *src = db_head();
	uint32_t check = (uint32_t) &src[index];
	if(check >= db_limit()) return 0;
	memcpy(rec, &src[index], sizeof(struct dbRecord));
	return 1;
}



char *process_output_table(int paragraph) // if 0 then begin
{
	struct dbRecord item;
	char abbrev[64], topic[64], comment[64];
	
	if(!paragraph) seeking = 0;
	
	if (!read_database(&item, seeking++)) return NULL;
	
	memcpy(abbrev, item.text, item.abbrev_len);
	abbrev[item.abbrev_len] = 0;
	
	memcpy(topic, &item.text[item.abbrev_len], item.topic_len);
	topic[item.topic_len]=0;
	
	memcpy(comment, &item.text[item.abbrev_len+item.topic_len], item.comment_len);
	comment[item.comment_len] = 0;
	
/*
	create portion of 512 bytes data for tcp
	stid mask format min max abbrev topic comment
	csv format
*/
    snprintf(strbuf, sizeof(strbuf)-1, 
		"0x%04X;0x%04X;%c;0x%04lX;0x%04lX;%s;%s;%s\r\n",
		item.stID, item.mask, item.format, item.min, item.max, 
		abbrev, topic, comment);	
	return strbuf;
}


int  write_database(struct dbRecord *rec, size_t index)
{
	size_t sz = sizeof(struct dbRecord);
	if(index*sz  > 65535) return 0;
	struct dbRecord *pages = (void *) CCMDATARAM_BASE;
	memcpy(&pages[index], rec, sz);
	return 1;
}



int process_input_table(char *buffer, int buflen)
{ // when < 0 or NULL then do nothing return 0 when no data
	if((buffer == NULL) || (buflen <= 0)) return 0;
	/*import data from buffer*/
		
	return 1;
}


void close_database(size_t index)
{ // finalize
}

static char line[512];
static size_t pos;
static void parse_incoming(void);

void console(void)
{
	for(;;)
	{
		if(!sd_Checking(&line[pos])) return;
		if((line[pos-1]=='\r') && (line[pos] == '\n'))
		{
			line[pos-1]=0;
			parse_incoming();
			pos = 0;
		}
		else
		{
			pos += 1;
			pos %= sizeof(line); 
		}
	}
}

void parse_incoming(void)
{
	switch(do_CGI(line, strlen(line)))
	{
		case cgi_Get:
		
		break;
		
		case cgi_Put:	
		break;
		
		case cgi_Post:	
		case cgi_None:	
		default:
		break;
	}
}


static int cmdtest(char *buf, size_t buflen, const char *term)
{
	size_t ls = strlen(term);
	int res = 0;
	if((buflen >= ls) && !strncmp(buf, term, ls)) res = ls;
	return res;
}

enum CGI_Command do_CGI(char *buf, size_t buflen)
{
	enum CGI_Command res= cgi_None;

	if(cmdtest(buf,buflen,"GET /")) res = cgi_Get;	
	else if(cmdtest(buf,buflen,"PUT /")) res = cgi_Put;	
	else if(cmdtest(buf,buflen,"POST /")) res = cgi_Post;	
	return res;
}

/* This is a header file addendum.h
 TITLE: This is a part of mqtt2 project
 LICENSE:

 * Copyright 2016-2023 drvmotor <100kwmore@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 Author: (c)drvmotor
 Date:03/29/23
*/
#ifndef _ADDENDUM_H_
#define _ADDENDUM_H_ 1
#include <stddef.h>
enum CGI_Command
{
	cgi_None = 0,
	cgi_Get,
	cgi_Post,
	cgi_Put
};

enum CGI_Command do_CGI(char *buf, size_t buflen);




int sd_Checking(char *b);
int process_input_table(char *buffer, int buflen); // when < 0 or NULL then do nothing return 0 when no data
char *process_output_table(int paragraph);
#endif


/* This is a source file can407.c
 TITLE: This is a part of mqtt_can project
 LICENSE:

 * Copyright 2016-2022 drvmotor <100kwmore@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 Author: (c)drvmotor
 Date:08/29/22
*/
#ifndef STM32f407xx
#define STM32F407xx
#endif
#include <stddef.h>
#include <string.h>
#include "nosys/stm32f4xx.h"
#include "can407.h"
#include "../prjconf.h"
#include "database.h"
#include "binhex.h"

#define NELEM(x) (sizeof(x)/sizeof(x[0]))

static void nosys_can_init_prim(CAN_TypeDef *port);
unsigned int pack_sent, pack_recv, err_mqttsub=0;

static void  can_loopback(CAN_TypeDef *port);
static void can1_post_init(void);

void nosys_can_init(int num)
{
	pack_recv = pack_sent = 0;
	switch(num)
	{
		case 1:
			RCC->APB1RSTR |= RCC_APB1RSTR_CAN1RST;
			RCC->APB1ENR |= RCC_APB1ENR_CAN1EN;
			RCC->APB1RSTR ^= RCC_APB1RSTR_CAN1RST;
			nosys_can_init_prim(CAN1);
			can1_post_init();
			//~ can_loopback(CAN1);
			break;
		case 2:
			RCC->APB1RSTR |= RCC_APB1RSTR_CAN2RST;
			RCC->APB1ENR |= RCC_APB1ENR_CAN2EN;
			RCC->APB1RSTR ^= RCC_APB1RSTR_CAN2RST;
			nosys_can_init_prim(CAN2);
			can_loopback(CAN2);
			break;
		default: break;
	}
}

static uint32_t get_my_clk(void)
{
	uint32_t res = 0;
	SystemCoreClockUpdate();
	res = SystemCoreClock;
	uint32_t k = RCC->CFGR & RCC_CFGR_PPRE1_Msk ;
	if(k == RCC_CFGR_PPRE1_DIV2) res /= 2;
	else if(k == RCC_CFGR_PPRE1_DIV4) res /= 4;
	else if(k == RCC_CFGR_PPRE1_DIV8) res /= 8;
	else if(k == RCC_CFGR_PPRE1_DIV16) res /= 16;
	return res;
}

/*exchange data*/
union CAN_Hdr
{
	uint32_t reg;
	struct CAN_Hdr_Bits_Exid
	{
		unsigned:1;
		unsigned	RTR:1;
		unsigned 	IDE:1;
		unsigned	ID:29;
	} ex;
	struct CAN_Hdr_Bits_Stid
	{
		unsigned :1;
		unsigned RTR:1;
		unsigned IDE:1;
		unsigned :18;
		unsigned ID:11;
	} st;
};

union CAN_Ctl
{
	uint32_t reg;
	struct CAN_Ctl_Bits
	{
		unsigned DLC:4;
		unsigned :4;
		unsigned FilterMask:8;
		unsigned TimeStamp:16;
	} bits;
};

union CAN_Data
{
	uint32_t bulk[2];
	uint8_t  bytes[8];
};

struct CAN_Packet
{
	union CAN_Hdr header;
	union CAN_Ctl control;
	union CAN_Data data;
};

/*Supported functions*/
static void set_CAN_Baudrate(CAN_TypeDef *xcan, uint32_t baud);
static void can_initRQ(CAN_TypeDef *xcan, char endis);

static int bxCAN_send(CAN_TypeDef *xcan, struct CAN_Packet *pkt, int mbox);
static int bxCAN_recv(struct CAN_Packet *dest, CAN_TypeDef *xcan, int mbox);

/*Filtering*/
struct bxCAN_Filter
{
	CAN_TypeDef 	*xcan;
	union CAN_Hdr 	compare;
	union CAN_Hdr 	mask;
	struct bxCAN_Filter_Opt
	{
		unsigned number:5;
		unsigned enable:1;
		unsigned assign:1;
		unsigned exact_list:1;
		unsigned mode32:1;
		unsigned half:1;
	} opt;
};


void can_loopback(CAN_TypeDef *port)
{
	can_initRQ(port, 1);
	port->BTR |= CAN_BTR_LBKM;
	can_initRQ(port, 0);
}

//~ static void bxCAN_filter(struct bxCAN_Filter *filter);
static void en_dis_CAN_recv(CAN_TypeDef *xcan, char endis);

void nosys_can_init_prim(CAN_TypeDef *port)
{
	port->MCR |= CAN_MCR_RESET;
	port->MCR &= (~(uint32_t)CAN_MCR_SLEEP);
	can_initRQ(port, 1);
	set_CAN_Baudrate(port, CAN_BAUD);

	can_initRQ(port, 0);
}

void can_initRQ(CAN_TypeDef *xcan, char endis)
{
	uint32_t ackmask = endis? CAN_MSR_INAK:0;

	if((xcan->MSR & CAN_MSR_INAK) == ackmask) return;
	xcan->MCR ^= CAN_MCR_INRQ;
	while((xcan->MSR & CAN_MSR_INAK) != ackmask);
}

static const struct CAN_B_TABLE
{
	uint32_t baud;
	uint32_t btr;
} can_table[]=
{
	{.baud = 100000, 	.btr = 0x001B001B},
	{.baud = 125000, 	.btr = 0x001C0014},
	{.baud = 250000, 	.btr = 0x001A000B},
	{.baud = 500000, 	.btr = 0x001A0005},
	{.baud = 1000000, 	.btr = 0x001A0002}
};

static void set_CAN_Baudrate(CAN_TypeDef *xcan, uint32_t baud)
{
	//~ uint32_t clk = get_my_clk();
	uint32_t btr = 0;
	for(uint8_t j = 0; j < NELEM(can_table); ++j)
	{
		if(can_table[j].baud == baud)
		{
			btr = can_table[j].btr;
			break;
		}
	}
	xcan->BTR &= ~(CAN_BTR_SJW|CAN_BTR_TS1|CAN_BTR_TS2|CAN_BTR_BRP);
	if(btr)
		xcan->BTR |= btr;
}

#define ALL_RECV_MSK ((7<<4)|(7<<1))

void en_dis_CAN_recv(CAN_TypeDef *port, char endis)
{
	if(endis)
	{//enable can reception irq
		port->IER |= ALL_RECV_MSK;
	}
	else
	{//disable can reception irq
		port->IER &= ~ALL_RECV_MSK;
	}
}



/*
send packet via specified mailbox, if mbox=0 then default available mailbox is used
*/
int bxCAN_send(CAN_TypeDef *xcan, struct CAN_Packet *pkt, int mbox)
{
	size_t code = (CAN1->TSR >> 24) & 3;
	if(mbox >= 0) code = mbox & 3;
	if(code > 2) return 0; //not ready
	if(!(xcan->TSR & (CAN_TSR_TME0 << code))) return 0;
	CAN_TxMailBox_TypeDef *ptx = &xcan->sTxMailBox[code];
	ptx->TDLR = pkt->data.bulk[0];	// fast copy
	ptx->TDHR = pkt->data.bulk[1];	//
	ptx->TDTR = pkt->control.reg & 0x0f;		// no timestamps, length of prepared
	ptx->TIR =  pkt->header.reg & ~1;
	ptx->TIR |= 1; // req xmit
	return 1;
}

static void get_pkt(struct CAN_Packet *dest, CAN_FIFOMailBox_TypeDef *mail)
{
	dest->header.reg =  mail->RIR;
	dest->control.reg = mail->RDTR;
	dest->data.bulk[0]= mail->RDLR;
	dest->data.bulk[1]= mail->RDHR;
}

int bxCAN_recv(struct CAN_Packet *dest, CAN_TypeDef *xcan, int mbox)
{
	if(mbox < 0)
	{//select the active mbox for polling
		if(xcan->RF0R & CAN_RF0R_FMP0) mbox = 0;
		else if(xcan->RF1R & CAN_RF1R_FMP1) mbox = 1;
	}

	switch(mbox)
	{
		case 0:
				if(!(xcan->RF0R & CAN_RF0R_FMP0)) return 0;
				get_pkt(dest, &xcan->sFIFOMailBox[0]);
				xcan->RF0R |= CAN_RF0R_RFOM0;
				break;
		case 1:
				if(!(xcan->RF1R & CAN_RF1R_FMP1)) return 0;
				get_pkt(dest, &xcan->sFIFOMailBox[1]);
				xcan->RF1R |= CAN_RF1R_RFOM1;
				break;
		default:
				return 0;
	}

	return 1;
}



/*serve data passed throuh CAN and publish it to MQTT*/
static struct myPKT_queue_s
{

	struct CAN_Packet array[256];
	uint8_t can_number[32];
	uint8_t rd,wr;
} packets, xmitter;


void can_serve(void)
{
	while(packets.rd != packets.wr)
	{ //publish it to mqtt
		volatile struct CAN_Packet *pack = &packets.array[packets.rd++];
		can2mqtt(&pack->data.bytes,
			pack->control.bits.DLC,
			pack->header.ex.IDE? pack->header.ex.ID:pack->header.st.ID,
			pack->header.ex.IDE); // scan db and select topic
	}

	while(xmitter.rd != xmitter.wr)
	{
		uint8_t nn = xmitter.can_number[xmitter.rd/8] & (1 << (xmitter.rd & 7));
		CAN_TypeDef *xcan = nn ? CAN2 : CAN1;
		if(!bxCAN_send(xcan, &xmitter.array[xmitter.rd], 0)) return;
		/*pkt sent*/
		xmitter.rd += 1;
	}
}

int can_send(int canN, uint8_t *data, uint8_t szdata, uint32_t ID, char isExID)
{
	if(canN == 1) xmitter.can_number[xmitter.wr/8] &= ~(1<<(xmitter.wr & 7));
	else if(canN == 2) xmitter.can_number[xmitter.wr/8] |= 1<<(xmitter.wr & 7);
	else return;

	struct CAN_Packet *pkt = &xmitter.array[xmitter.wr];
	memset(pkt, 0, sizeof(struct CAN_Packet));

	if(isExID)
	{
		pkt->header.ex.ID = ID;
		pkt->header.ex.IDE = 1;
	}
	else pkt->header.st.ID = (uint16_t)ID;

	if(szdata > 8) szdata = 8; // truncate
	pkt->control.bits.DLC = szdata;

	for(uint8_t j=0; j < szdata;++j)
	{
		pkt->data.bytes[j]=data[j];
	}
	xmitter.wr += 1;
}



#ifdef CAN2_ENABLED
void can2test(void)
{
	uint8_t testdata[]={0,1,2,3,4};
	can_send(CAN2, testdata, sizeof(testdata), 0x345, 0);
}
#endif

/*isr handle*/
void can1_post_init(void)
{
	CAN_TypeDef *can = CAN1;
	//set filter
	can->FMR |= CAN_FMR_FINIT;
	/*pass filter to RX0*/
	can->FM1R = 0; //mask mode 32 bit
	can->FFA1R = 0; // assignment to 0 fifo
	can->FS1R |= 1; //32 bit mask
	can->FA1R &= 0xF0000000;
	can->sFilterRegister[0].FR1=0;
	can->sFilterRegister[0].FR2=0;
	can->FA1R |= 1; // active
	can->FMR &= ~CAN_FMR_FINIT;//apply

	//enable nvic
	NVIC_EnableIRQ(CAN1_RX0_IRQn);
	//enable hw receiver
	CAN1->IER |= CAN_IER_FMPIE0;
}

#include "stm32_isr.h"
#include "ch.h"
void __attribute__((used)) STM32_CAN1_RX0_HANDLER (void)
{
	CH_IRQ_PROLOGUE();
	chSysLockFromIsr();
	bxCAN_recv(&packets.array[packets.wr++], CAN1, 0);
	chSysUnlockFromIsr();
	CH_IRQ_EPILOGUE();
}


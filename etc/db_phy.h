/* This is a header file db_phy.h
 TITLE: This is a part of mqtt2 project
 LICENSE:

 * Copyright 2016-2023 drvmotor <drvmotor@i.ua>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 Author: (c)drvmotor
 Date:04/11/23
*/
#ifndef _DB_PHY_H_
#define _DB_PHY_H_ 1
#include <stddef.h>
#include <stdint.h>

int 		db_open_read(void); // 0 if no data
void		*db_read_item(size_t item); // if no data then null
void	 	db_write_item(void *data, size_t sz, int seek_zero);
int 	 	db_write_close(void); // place new image in flash

size_t 		db_get_items_to_wr(size_t el_size);
void		*db_get_image(void);

#endif


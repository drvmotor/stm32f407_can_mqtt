/*	Support	files	for	GNU	libc.	Files	in	the	system	namespace	go	here.
	Files	in	the	C	namespace	(ie	those	that	do	not	start	with	an
	underscore)	go	in	.c.	*/

//#include	<_ansi.h>
#include	<sys/types.h>
#include	<sys/stat.h>
#include	<sys/fcntl.h>
#include	<stdio.h>
#include	<string.h>
#include	<time.h>
#include	<sys/time.h>
#include	<sys/times.h>
#include	<errno.h>
#include	<unistd.h>
#include	<sys/wait.h>

#undef	errno
extern int	errno;


void	initialise_monitor_handles(void)
{
}

int	_getpid(void)
{
	return	1;
}

int	_kill(int	pid,int	sig)
{
	errno	=	EINVAL;
	return	-1;
}

void	_exit	(int	status)
{
	_kill(status,	-1);
	while	(1);
}





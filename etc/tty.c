/* This is a source file tty.c
 TITLE:
 LICENSE:

 * Copyright 2016-2023 drvmotor <drvmotor@i.ua>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 Author: (c)drvmotor
 Date:04/11/23
*/

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "ch.h"
#include "hal.h"

#include "chprintf.h"
#include "database.h"
#include "prjconf.h"


static char line[512];
static size_t pos;
extern int sd_Checking(char *b);
static void parse_incoming(char *buf);
static char prev;
void console(void)
{
	for(;;)
	{
		char c;
		if(!sd_Checking(&c)) return;
		if((prev=='\r') && (c == '\n'))
		{
			line[pos]=0;
			parse_incoming(line);
			pos = prev = 0;
		}
		else
		{
			prev = line[pos++] = c;
			pos %= sizeof(line);
		}
	}
}

void parse_incoming(char *buf)
{
	size_t index=0;
	switch(db_request(buf, &index))
	{
		case rq_get:
		{
			char *s = db_get_item(atoi(&buf[index]));
			myprintf("$>%s", s);//prompt means that this is config
		}
			break;
		case rq_put:
		{
			char *s = db_put_item(&buf[index]);
			myprintf("$>%s", s);
		}
			break;
		case rq_fin:
			db_finalize();
			break;
		case rq_none:
		default:
			break;
	}
}

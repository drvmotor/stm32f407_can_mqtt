/* This is a source file binhex.c
 TITLE: This is a part of mqtt2 project
 LICENSE:

 * Copyright 2016-2023 drvmotor <drvmotor@i.ua>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 Author: (c)drvmotor
 Date:04/12/23
*/

#include <ctype.h>
#include "binhex.h"


char fast_binhex4(uint8_t b)
{
	b &= 0x0f;
	if(b < 10) b += '0';
	else
	{
		b += 'A' - 10;
	}
	return b;
}

char *fast_binhex8(char *txt, uint8_t byte)
{
	*txt++ = fast_binhex4(byte >>4);
	*txt++ = fast_binhex4(byte);
	return txt;
}

char *fast_binhex16(char *txt, uint16_t w)
{
	txt = fast_binhex8(txt, w >> 8);
	txt = fast_binhex8(txt, w );
	return txt;
}

char *fast_binhex32(char *txt, uint32_t w)
{
	txt = fast_binhex16(txt, w>>16);
	txt = fast_binhex16(txt, w);
	return txt;
}


uint8_t fast_hexbin4(char c)
{
	return (isdigit(c))? (c - '0') : (c - 'A' + 10);
}

char * fast_hexbin8(char *txt, uint8_t *val)
{
	while(isspace((int)*txt)) txt++;
	uint8_t x1 = fast_hexbin4(*txt++);
	uint8_t x2 = fast_hexbin4(*txt++);
	*val = (x1 << 4) + x2;
	return txt;
}

char * fast_hexbin16(char *txt, uint16_t *val)
{
	uint8_t x1, x2;
	txt = fast_hexbin8(txt, &x1);
	txt = fast_hexbin8(txt, &x2);
	*val = x1; *val <<= 8; *val += x2;
	return txt;
}

char * fast_hexbin32(char *txt, uint32_t *val)
{
	uint16_t x1, x2;
	txt = fast_hexbin16(txt, &x1);
	txt = fast_hexbin16(txt, &x2);
	*val = x1; *val <<= 16; *val += x2;
	return txt;
}

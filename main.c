/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
#include <stdarg.h>
#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"

#include "lwipthread.h"

#include "etc/web.h"
#include "mqtt/mqtt.h"
#include "etc/can407.h"

#include "prjconf.h"

/*
 * Green LED blinker thread, times are in milliseconds.
 */


static WORKING_AREA(waThread1, 128);

extern void console(void);

int sd_Checking(char *b)
{
	return sdAsynchronousRead(&SD6, b, 1);
}


static msg_t Thread1(void *arg) {
	unsigned int rcv=100500, sent=100500, err=100500;

  (void)arg;
  chRegSetThreadName("blinker");
  palSetPadMode(BLINKER_PORT, BLINKER_PIN, PAL_MODE_OUTPUT_PUSHPULL);
  palSetPadMode(BLINKER_PORT, MQTT_ALARM_PIN, PAL_MODE_OUTPUT_PUSHPULL);
  palSetPad(BLINKER_PORT, MQTT_ALARM_PIN);

	for(;;)
	{
		palClearPad(BLINKER_PORT, BLINKER_PIN);
		chThdSleepMilliseconds(500);
		palSetPad(BLINKER_PORT, BLINKER_PIN);
		chThdSleepMilliseconds(500);
	}
}

/*
util
*/

void  myprintf(const char *fmt, ... )
{
	va_list ap;
	va_start(ap, fmt);
	chvprintf((void *)&SD6, fmt, ap);
	va_end(ap);
}

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Activates the serial driver 6 using the driver default configuration.
   */
  sdStart(&SD6, NULL);
  palSetPadMode(GPIOC, 6, PAL_MODE_ALTERNATE(8));
  palSetPadMode(GPIOC, 7, PAL_MODE_ALTERNATE(8));

  chprintf(&SD6, "Perform CAN init\r\n");

#ifdef CAN1_ENABLED
	palSetPadMode(GPIOD, 0, PAL_MODE_ALTERNATE(9));
	palSetPadMode(GPIOD, 1, PAL_MODE_ALTERNATE(9));
	nosys_can_init(1);
#endif

#ifdef CAN2_ENABLED
	palSetPadMode(GPIOB, 5, PAL_MODE_ALTERNATE(9));
	palSetPadMode(GPIOB, 6, PAL_MODE_ALTERNATE(9));
	nosys_can_init(2);
#endif

  /*
   * Creates the blinker thread.
   */
  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);
  //~ chThdCreateStatic(waConsole, sizeof(waConsole), NORMALPRIO, Thread2, NULL);

  /*
   * Creates the LWIP threads (it changes priority internally).
   */
  chThdCreateStatic(wa_lwip_thread, LWIP_THREAD_STACK_SIZE, NORMALPRIO + 1,
                    lwip_thread, NULL);

  /*
   * Creates the HTTP thread (it changes priority internally).
   */
  chThdCreateStatic(wa_http_server, sizeof(wa_http_server), NORMALPRIO + 1,
                    http_server, NULL);

  /*
   * Creates the MQTT Client thread (it changes priority internally).
   */
  chThdCreateStatic(wa_mqtt_client, sizeof(wa_mqtt_client), NORMALPRIO + 1,
                    mqtt_client, NULL);


	/*
		can functioning from normal thread activity
	*/
	for(;;)
	{
		can_serve();
		console();
		//chThdSleepMilliseconds(100);
		chThdSleepMilliseconds(10);
	}
}


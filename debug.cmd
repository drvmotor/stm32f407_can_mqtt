set radix 16
define connect
target remote localhost:4242
end

define reload
file build/ch.elf
end

define post
p (char *)0x20000000
end

define ccm
p (struct dbRecord *) 0x10000000
end

define scb
p *((SCB_Type *)0xE000E100)
end

define can
p *((CAN_TypeDef *)0x40006400)
end

define can2
p *((CAN_TypeDef *)0x40006800)
end
